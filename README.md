php5-nginx-vagrant-sample
=========================

---

yandodさんの https://github.com/yandod/php5-nginx-vagrant-sample を個人利用のためforkしたものです。

---

Vagrantを使ってPHPとMySQLが動作する環境を自動で設定します。OSなどに依存しないXAMPP/MAMPのようなものと考えてください。

<table>
<tr>
<th>OS</th>
<td>Ubuntu 12.04 LTS (GNU/Linux 3.2.0-23-generic x86_64)</td>
</tr>
<tr>
<th>PHP</th>
<td>PHP 5.3.10-1ubuntu3.6 with Suhosin-Patch</td>
</tr>
<tr>
<th>Nginx</th>
<td>1.1.19</td>
</tr>
<tr>
<th>MySQL</th>
<td>5.5.31-0ubuntu0.12.04.2 (Ubuntu)</td>
</tr>
</table>


PHPカンファレンス関西2013の講演で使ったコードなど

スライドはこちらです。
[Chef + Vagrantで作るこれからの開発環境とクラウド](https://speakerdeck.com/yandod/chef-plus-vagrantdezuo-rukorekarafalsekai-fa-huan-jing)


## このVagrantの使い方

### VirtualBoxをダウンロードしてインストール
VirtualBoxをセットアップしてください。
[https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

### Vagrantをダウンロードしてインストール
最新版をインストールしてください。
[http://downloads.vagrantup.com/](http://downloads.vagrantup.com/)

### このリポジトリをクローン
gitでクローンするか、ZIPなどでダウンロードしてください。
<pre>
git clone git://github.com/kumamidori/php5-nginx-vagrant-sample.git
</pre>

### ソースコードの確認
./sandbox.local下に phpinfo() を出力するプログラムが同梱されています。

### vagrantを起動
クローンしたリポジトリ内でvagrantを起動。初回のみOSのダウンロードに時間がかかります。
<pre>
cd php5-nginx-vagrant-sample
vagrant up
</pre>

### 動作確認
http://127.0.0.1:8080 にアクセスする。IPアドレスを変更する場合はVagrantfileを編集します。
またサーバ内にSSHしたい場合はvagrantコマンドを使います。

<pre>
vagrant ssh
</pre>

### 終了
vagrantコマンドで仮想マシンを終了、又は破棄出来ます。

一旦止めるだけの場合。
<pre>
vagrant halt
</pre>

データを破棄する場合。次回、<code>vagrant up</code>の際にはまっさらなマシンが作成される。
<pre>
vagrant destroy
</pre>


## このVagrantの使い方

### 同梱されているApache の VirtualHost

下記3つのVirtualHost が有効になった状態でApacheが起動しています。

```
<VirtualHost *:80>
  ServerName sandbox.local
  DocumentRoot /vagrant/sandbox/
</VirtualHost>

<VirtualHost *:80>
  ServerName hello.local
  DocumentRoot /vagrant/hello.local/htdocs
</VirtualHost>

<VirtualHost *:80>
  ServerName test.local
  DocumentRoot /vagrant/test.local/htdocs
</VirtualHost>
```

詳細は、下記手順で確認できます。

```
ゲストOSから vagrant ssh でホストOSにログインする。
$ vagrant@precise64:/vagrant$ /usr/sbin/apache2ctl -S
```

### ソースコードの配置

./sandbox.local 以下に動作させたいPHPのソースコードなどを置くか、または、Vagrantfileの設定を書き換えて任意のディレクトリを指します。
これでソースコードが仮想マシン内に同期するのでローカルで編集できます。
